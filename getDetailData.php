<?php
   function getDetailData($url) {
       $html = file_get_html($url);
       $content = '';
       foreach ($html->find('table tr td table tr') as $element) {
           $obj = new simple_html_dom();
           $dom = $obj->load($element);
           $count = count($dom->find('td'));
           switch ($count ) {
               case 1:
                   $content .= $dom->find('td', 0)->plaintext;
                   $content .= '</br>';
                   break;
               case 2:
                   $content .= $dom->find('td', 0)->plaintext;
                   $content .=  ':';
                   $content .= $dom->find('td', 1)->plaintext;
                   $content .= '</br>';
                   break;
               case 4:
                   $content .= $dom->find('td', 0)->plaintext;
                   $content .=  ':';
                   $content .= $dom->find('td', 1)->plaintext;
                   $content .= '|';
                   $content .= $dom->find('td', 2)->plaintext;
                   $content .=  ':';
                   $content .= $dom->find('td', 3)->plaintext;
                   $content .= '</br>';
           }
       }
           if (!is_dir(date('yy'))) {
               mkdir(date('yy'));
           }
           if (!is_dir(date('yy').'/'.date('m').date('d'))) {
               mkdir(date('yy').'/'.date('m').date('d'));
           }
           $myfile = fopen(date('yy').'/'.date('m').date('d').'/'.date('H').'_'.date('i')."_detail.html", "w");
           fwrite($myfile, $content);
           fclose($myfile);
   }
?>

