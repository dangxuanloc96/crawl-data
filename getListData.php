<?php
function getListData($url) {
    $ch = curl_init();
    $user_agent='Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_AUTOREFERER, false);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_USERAGENT, $user_agent);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_SSLVERSION,CURL_SSLVERSION_DEFAULT);
    $webcontent= curl_exec ($ch);
    $error = curl_error($ch);
    curl_close ($ch);
    $obj = new simple_html_dom();
    $dom = $obj->load($webcontent);

    $tableInfo =  $dom->find('table',1);
    $objTable = new simple_html_dom();
    $domTable = $objTable->load($tableInfo);
    $content = '';
    foreach ($domTable->find('tr') as $tr) {
        $objTr = new simple_html_dom();
        $domTr = $objTr->load($tr);
        foreach ($domTr->find('td') as $td) {
            $content .=  $td->plaintext;
        }
        $content .= '</br>';
    }

    if (!is_dir(date('yy'))) {
        mkdir(date('yy'));
    }
    if (!is_dir(date('yy').'/'.date('m').date('d'))) {
        mkdir(date('yy').'/'.date('m').date('d'));
    }
    $myfile = fopen(date('yy').'/'.date('m').date('d').'/'.date('H').'_'.date('i')."_list.html", "w");
    fwrite($myfile, $content);
    fclose($myfile);
}
?>